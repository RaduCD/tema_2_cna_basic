#pragma once
#include<string>
#include"Date.h"
class Sign
{
private:

	Date* dateFrom;
	Date* dateTo;
	std::string sign;

public:

	Sign(Date* dateFrom, Date* dateTo, std::string sign);
	Date* getDateFrom();
	Date* getDateTo();
	std::string getSign();



};

