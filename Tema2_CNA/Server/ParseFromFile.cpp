#include "ParseFromFile.h"

ParseFromFile* ParseFromFile::getInstance()
{
	if (instance == nullptr)
		instance = new ParseFromFile();

	return instance;
}


std::vector<Sign*> ParseFromFile::initListSign()
{

	short day;
	short mounth;
	std::string sign;

	std::string line;
	
		std::ifstream f("file.txt");
		while (!f.eof())
		{
			std::getline(f, line);
			f >> mounth;
			f >> day;
			Date* dateFrom = new Date(mounth, day);
			f >> mounth;
			f >> day;
			Date* dateTo = new Date(mounth, day);
			f >> sign;
			listSigns.push_back(new Sign(dateFrom, dateTo, sign));
		}
		return listSigns;
}

std::string ParseFromFile::findTheSignFromDate(Date *requestDate)
{
	std::string sign = "";

	if (requestDate->getMounth() == 1 && requestDate->getDay() <= 19 || requestDate->getMounth() == 12 && requestDate->getDay() >= 21 && requestDate->getDay() <= 31)
	{
		sign = "Capricorn";
		return sign;
	}

	for (int index = 0; index < listSigns.size(); ++index)
		if (listSigns[index]->getDateFrom()->getMounth() <= requestDate->getMounth() && requestDate->getMounth() <= listSigns[index]->getDateTo()->getMounth())
		{
			int dayRequestDate = requestDate->getDay();
			
			if (listSigns[index]->getDateFrom()->getDay() <= dayRequestDate && limitsForEachMounths[listSigns[index]->getDateFrom()->getMounth() - 1] ||
				1 <= dayRequestDate && dayRequestDate <= listSigns[index]->getDateTo()->getDay())
			{
				sign = listSigns[index]->getSign();
				break;
			}
		}
	return sign;
}
