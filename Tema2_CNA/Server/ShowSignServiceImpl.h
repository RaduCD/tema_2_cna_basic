#pragma once
#include<sstream>
#include "ShowSignOperation.grpc.pb.h"

class ShowSignServiceImpl  final : public ShowSignOperationService::Service
{

public:
	
	void parseMounthDay(std::string requestDate, short& mounth, short& day);

	class ShowSignServiceImpl() {};

	::grpc::Status ShowTheSign(::grpc::ServerContext* context, const ::ShowSignRequest* request, ::ShowSign* response) override;
};


