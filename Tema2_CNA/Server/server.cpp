#include"ShowSignServiceImpl.h"

#include<grpc/grpc.h>
#include<grpcpp/server.h>
#include<grpcpp/server_builder.h>
#include<grpcpp/server_context.h>
#include"ParseFromFile.h"

ParseFromFile* ParseFromFile::instance = 0;


int main()
{
   
    std::string server_address("localhost:1234");
    ShowSignServiceImpl service;

    ::grpc_impl::ServerBuilder serverBuilder;
    serverBuilder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
    serverBuilder.RegisterService(&service);
    std::unique_ptr<::grpc_impl::Server> server(serverBuilder.BuildAndStart());

    std::vector<Sign*> listSigns = ParseFromFile::getInstance()->initListSign();


    std::cout << "Server listening on " << server_address << std::endl;
    server->Wait();
}
